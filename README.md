#QQChatBot

一、介绍

QQChatBot 基于开源项目ChatterBot和QQBot而成，是一款QQ聊天机器人

二、安装方法

在 Python 3.4/3.5 下使用，用 pip3 安装：

pip3 install chatterbot hug requests pyqrcode pypng Pillow qqbot

三、使用方法

1. 启动 start.sh

2. python3 aiyun.py

四、ChatterBot 中文语料库位置

python3.5/dist-packages/chatterbot/corpus/data/chinese

五、更新内容

1.实现QQ聊天机器人私聊

2.只有被群内其他成员 @ 时才会回复

六、参考资料

QQChatBot 基于以下开源项目：

https://github.com/gunthercox/ChatterBot

https://github.com/pandolia/qqbot

七、问题反馈

有任何问题或建议可以发邮件给我 aiyun@zjzm.wang 
