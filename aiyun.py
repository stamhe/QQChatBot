#!/usr/bin/env python
# coding: utf-8

from qqbot import QQBotSlot as qqbotslot, RunBot
import requests
bot_api="http://127.0.0.1:8000/get_response"


@qqbotslot
def onQQMessage(bot, contact, member, content):  
     if '@ME' in content:       
            user_input = content
            payload={"user_input":user_input}
            response = requests.get(bot_api,params=payload).json()["response"]
            bot.SendTo(contact, member.name+'，'+response)
     elif contact.ctype == 'buddy':
            user_input = content
            payload={"user_input":user_input}
            response = requests.get(bot_api,params=payload).json()["response"]
            bot.SendTo(contact, response)
     #else:
            #bot.SendTo(contact, 'aiyun') 
RunBot()
